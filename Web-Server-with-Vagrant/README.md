# Webserver auf TBZ-Cloud mit Vagrant deklarativ aufsetzen

## Voraussetzungen:


* Vagrant installiert
* Virtualbox und Extension Pack (gleiche Version) installiert 
(Es kann auch ein anderer Provider wie z.B. VMware benutzt werden - Default ist Virtualbox)
* Gitlab Account
* Windows: GitBash auf dem lokalen Host installiert
* Mac oder Linux: (Bash bereits vorhanden, sonst ebenfalls auf GitBash) verfügbar
* Editor: z.B: Visual Studio Code , Atom oder Sublime Text etc...
------

## 1. Vagrant Einstieg

------

## In die TBZ Cloud wechseln 


GitBash oder Bash-Terminal öffnen.  

Checken, in welchem Verzeichnis du bist:

    pwd   
  
Verbindung auf die TBZ-VM:  

    ssh ubuntu@10.3.44.27
 

Passwort eingeben (http://10.3.44.27/data/.ssh/passwd)   

------
### Preflight Checks 

`vagrant -v`  
Checken, welche Vagrant-Version installiert ist  
`vboxmanage -v`  
checken, welche Virtualbox-Version installiert ist   
`ssh`  
Checken, ob SSH installiert ist

------

### Setup erstes Projekt (Ubuntu-VM)

Ins Verzeichnis des vorgesehenen Projektes wechseln:

    cd<Projekt-Verzeichnis>  
Kontrolle, ob im richtigen Verzeichnis: 

    pwd  
 
  Projektverzeichnis "ubuntu" anlegen:  

    mkdir ubuntu-web-server 

ins Verzeichnis "ubuntu-web-server" wechseln:

    cd ubuntu-web-server  


 
Im aktuellen Verzeichnis ein Vagrantfile (für das OS Ubuntu Precise32) erstellen:  

    vagrant init hashicorp/precise32 

Überprüfen, ob das Vagrantfile vorhanden ist:

    ls -al


Vagrantfile überprüfen, ob die beiden Zeilen vorhanden sind:

![images](images/Vagrantfile.png)
`Vagrant.configure("2") do |config|`  
und   
`config.vm.box = "hashicorp/precise32"`  

------
### VM starten und überprüfen

Virtualbox-VM mit Vagrant starten:

    vagrant up 

In die VM "hüpfen" und überprüfen:

    vagrant ssh  

`uname -a`  
Checken, ob Distro stimmt --> Ubuntu  

`df -h`  
Diskfree Human-readable

------
### VM vom Host aus überprüfen

Aus der VM zurück auf den Host:  

    exit  

Checken, welche Virtualbox-VMs am Laufen sind:

    vboxmanage list runningvms 

------
### Files zwischen Host und VM sharen

„Synced Folders“ Verzeichnisse und Files können zwischen dem Host-System und den VM’s (Guests) ge’shared‘ werden.

Auf dem Host-System eine Datei mit Inhalt erstellen:

     touch hello.txt

Dann wieder auf die Guest-VM verbinden:

    vagrant ssh

Ins Vagrant-Verzeichnis wechseln:

    cd /vagrant 
    
Dieses Verzeichnis ist mit dem Gastsystem ge'sync'ed

    ls -al  

Hier ist das vorher erstellte hello.txt ersichtlich

------
## Nützliche Befehle

VM mit "halt" runterfahren (abschalten)

    vagrant halt


VM mit "up" starten

    vagrant up

VM mit "suspend" anhalten/einfrieren. Free up Memory und CPU. (Z.B. falls ich knapp an Ressourcen bin, weil z.B. noch andere VMs laufen)

    vagrant suspend


VM mit "resume" wieder reaktivieren.Geht schneller, als wenn VM frisch gestartet wird

    vagrant resume

Sämtliche VBox-Eingabemöglichkeiten auflisten (Virtualbox-Befehl)

    VBox


Überprüfen, welche VMs gerade laufen (Virtualbox-Befehl)

    VBoxManage.exe list vms

VM löschen (zerstören)Kann beliebig angewendet werden. Einer der grössten Vorteile von Vagrant. Genau diese VM kann später mit nur einem Kommando (vagrant up) wieder genau gleich hergestellt werden, wie vor dem Löschen (alle notwendigen Schritte dazu sind im Vagrantfile deklariert)

    vagrant destroy (-f)


Vagrant Hilfe (Help)
Hilfe zu sämtlichen Vagrant-Kommandos kann wie folgt bezogen werden

    vagrant -h
    vagrant --help

Beim Kommando $ vagrant status kann zusätzlich noch der Log-Level definiert werden (untersch. Outputs). Das ist nützlich, wenn Probleme auftreten und diese mit den aktuellen Settings nicht erkennbar sind. Wir unterscheiden zwischen folgenden Status:

* debug  
* info (normal)  
* warn  
* error  

Status wie folgt überprüfen und bei Bedarf anders setzen:

    vagrant status  
Defaultwert ist "Info"

    export VAGRANT_LOG=debug 
 Ändern, um mehr Infos zu erhalten

Es ist aber auch möglich, den Status der Variable zu ändern, ohne Variable fix zu setzen; und zwar wie folgt:

    vagrant status --debug
nur 1x, nicht persistent


------
# 2. NGINX Webserver deklarativ aufsetzen

**Vorbereitung**  
Zuerst erstellen wir für den NGINX-Webserver ein neues Verzeichnis im GIT-Repo

    git clone git@gitlab.com:kevin.milic-lukic/tbz-public-repo.git

ins richtige Directory wechseln:

    cd <Projektverzeichnis>  
Verzeichnis erstellen:

    $ mkdir nginx  

Ins Verzeichnis wechseln

    cd nginx  

**Projekt initialisieren / OS (Vagrant Box) ergänzen**  

In diesem Fall initialisieren wir das Vagrant-Projekt mit dem Minimal-Flag, um die vielen auskommentierten Zeilen im Vagrantfile rauszulöschen und später eigene Inhalte einzufügen. Zusätzlich muss noch ein OS (in Vagrant "Box" genannt) angegeben werden. Wir entscheiden uns für die bereits früher verwendete Ubuntu-Box. Da wir diese ge'downloaded' haben, ist sie bereits lokal vorhanden. Andernfalls muss sie im Vagrant-Repository geholt werden (dauert dann etwas länger)

Initialisieren mit der Ubuntu-Box

    vagrant init hashicorp/xenial64 --minimal


**Vagrantfile ergänzen**  

Bevor die VM zum erstem Mal gestartet wird, ergänzen wir das Config-File noch mit einem Eintrag. Dazu öffnen wir das Vagrantfile mit einem beliebigen Editor (in diesem Fall mit **nano**) und geben der VM den Namen "web-dev". Dieser VM-Name ist frei wählbar und erscheint später, wenn wir in die VM "hüpfen", auch im Prompt (siehe übernächstes Bild)

![image](images/Vagrantfile_Hostname-config.png)

**VM zum ersten Mal starten und 
checken**  

VM starten

    vagrant up

anschliessend in die VM "hüpfen" und überprüfen, ob der im Vagrantfile definierte Systemname angezeigt wird

in die VM "hüpfen":

    vagrant ssh  